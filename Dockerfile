FROM 514672819900.dkr.ecr.ap-south-1.amazonaws.com/kiranbaseimage:31
WORKDIR /tmp/jenkins/workspace/CI_PipelineJob
ADD samplewar/target/samplewar.war /opt/jboss/wildfly/standalone/deployments/
RUN /opt/jboss/wildfly/bin/add-user.sh admin admin --silent
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]
